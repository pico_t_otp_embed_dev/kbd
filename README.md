# Keyboard Library for Embassy HID (Rust)

This library provides a simple and easy-to-use interface for sending keyboard input using the Embassy HID library in Rust. It also includes functionality for translating strings into keycodes for more complex input to computer.