#![no_std]
#![no_main]

use defmt::warn;

use embassy_rp::peripherals::USB;
use embassy_rp::usb::Driver;
use embassy_usb::class::hid::HidWriter;

use usbd_hid::descriptor::KeyboardReport;

use kbd_frca::FrCaKbd;
use kbd_us::UsKbd;
use key_codes::KeyCode;

pub mod kbd_frca;
pub mod kbd_us;
pub mod key_codes;

pub trait KbdType {
    fn char_to_kr(&self, c: char) -> Option<KeyboardReport>;
    fn is_altgr_modifier(&self, c: char) -> bool;
    fn is_shift_diaeresis_modifier(&self, c: char) -> bool;
    fn is_shift_cedilla_modifier(&self, c: char) -> bool;
}

#[repr(u8)]
pub enum KeyboardLayout {
    Us,
    FrCa,
}

#[derive(Clone, Copy)]
pub struct InnerKr {
    keycodes: [KeyCode; 2],
    modifier: u8,
}

#[repr(u8)]
pub enum Keyboard {
    Us(UsKbd),
    FrCa(FrCaKbd),
}

impl KbdType for Keyboard {
    fn char_to_kr(&self, c: char) -> Option<KeyboardReport> {
        match self {
            Keyboard::Us(kb_us) => kb_us.char_to_kr(c),
            Keyboard::FrCa(kb_fr) => kb_fr.char_to_kr(c),
        }
    }

    fn is_altgr_modifier(&self, c: char) -> bool {
        match self {
            Keyboard::Us(kb_us) => kb_us.is_altgr_modifier(c),
            Keyboard::FrCa(kb_frca) => kb_frca.is_altgr_modifier(c),
        }
    }

    fn is_shift_diaeresis_modifier(&self, c: char) -> bool {
        match self {
            Keyboard::Us(kb_us) => kb_us.is_shift_diaeresis_modifier(c),
            Keyboard::FrCa(kb_frca) => kb_frca.is_shift_diaeresis_modifier(c),
        }
    }

    fn is_shift_cedilla_modifier(&self, c: char) -> bool {
        match self {
            Keyboard::Us(kb_us) => kb_us.is_shift_cedilla_modifier(c),
            Keyboard::FrCa(kb_frca) => kb_frca.is_shift_cedilla_modifier(c),
        }
    }
}

impl Keyboard {
    pub fn new(layout: KeyboardLayout) -> Self {
        match layout {
            KeyboardLayout::Us => Keyboard::Us(UsKbd),
            KeyboardLayout::FrCa => Keyboard::FrCa(FrCaKbd),
        }
    }

    // Send string to keyboard as a series of KerboardReport
    pub async fn str_to_keyboard(
        &self,
        writer: &mut HidWriter<'static, Driver<'static, USB>, 8>,
        s: &str,
    ) {
        let empty_kr = KeyboardReport {
            keycodes: [0, 0, 0, 0, 0, 0],
            leds: 0,
            modifier: 0,
            reserved: 0,
        };

        for c in s.chars() {
            if let Some(report) = self.char_to_kr(c) {
                if self.is_altgr_modifier(c) {
                    let modifier = KeyboardReport {
                        keycodes: [KeyCode::Slash as u8, 0, 0, 0, 0, 0],
                        leds: 0,
                        modifier: KeyCode::RightAlt.modifier_bitmask().unwrap(),
                        reserved: 0,
                    };
                    match writer.write_serialize(&modifier).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                    match writer.write_serialize(&empty_kr).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                    match writer.write_serialize(&report).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                    match writer.write_serialize(&empty_kr).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                } else if self.is_shift_diaeresis_modifier(c) {
                    let modifier = KeyboardReport {
                        keycodes: [KeyCode::RightSquareBracket as u8, 0, 0, 0, 0, 0],
                        leds: 0,
                        modifier: KeyCode::LeftShift.modifier_bitmask().unwrap(),
                        reserved: 0,
                    };
                    match writer.write_serialize(&modifier).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                    match writer.write_serialize(&empty_kr).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                    match writer.write_serialize(&report).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                    match writer.write_serialize(&empty_kr).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                } else if self.is_shift_cedilla_modifier(c) {
                    let modifier = KeyboardReport {
                        keycodes: [KeyCode::RightSquareBracket as u8, 0, 0, 0, 0, 0],
                        leds: 0,
                        modifier: 0,
                        reserved: 0,
                    };
                    match writer.write_serialize(&modifier).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                    match writer.write_serialize(&empty_kr).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                    match writer.write_serialize(&report).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                    match writer.write_serialize(&empty_kr).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                } else {
                    // Send the normal keyboard report.
                    match writer.write_serialize(&report).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                    match writer.write_serialize(&empty_kr).await {
                        Ok(()) => {}
                        Err(e) => warn!("Failed to send report: {:?}", e),
                    };
                }
            }
        }
    }
}
