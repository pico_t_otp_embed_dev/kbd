use heapless::String;
use usbd_hid::descriptor::KeyboardReport;

use super::{InnerKr, KbdType, KeyCode};

pub struct UsKbd;

impl KbdType for UsKbd {
    fn char_to_kr(&self, c: char) -> Option<KeyboardReport> {
        const E: KeyCode = KeyCode::Empty;

        #[allow(non_snake_case)]
        let LSHIFT: u8 = KeyCode::LeftShift.modifier_bitmask().unwrap();

        #[rustfmt::skip]
        let inner_kr = match c {

            // Space
            ' ' => Some( InnerKr { keycodes: [KeyCode::Space, E], modifier: 0 }),

            // 0 to 9
            '0' => Some( InnerKr { keycodes: [KeyCode::Num0, E], modifier: 0 }),
            '1' => Some( InnerKr { keycodes: [KeyCode::Num1, E], modifier: 0 }),
            '2' => Some( InnerKr { keycodes: [KeyCode::Num2, E], modifier: 0 }),
            '3' => Some( InnerKr { keycodes: [KeyCode::Num3, E], modifier: 0 }),
            '4' => Some( InnerKr { keycodes: [KeyCode::Num4, E], modifier: 0 }),
            '5' => Some( InnerKr { keycodes: [KeyCode::Num5, E], modifier: 0 }),
            '6' => Some( InnerKr { keycodes: [KeyCode::Num6, E], modifier: 0 }),
            '7' => Some( InnerKr { keycodes: [KeyCode::Num7, E], modifier: 0 }),
            '8' => Some( InnerKr { keycodes: [KeyCode::Num8, E], modifier: 0 }),
            '9' => Some( InnerKr { keycodes: [KeyCode::Num9, E], modifier: 0 }),

            // a to z
            'a' => Some( InnerKr { keycodes: [KeyCode::A, E], modifier: 0 }),
            'b' => Some( InnerKr { keycodes: [KeyCode::B, E], modifier: 0 }),
            'c' => Some( InnerKr { keycodes: [KeyCode::C, E], modifier: 0 }),
            'd' => Some( InnerKr { keycodes: [KeyCode::D, E], modifier: 0 }),
            'e' => Some( InnerKr { keycodes: [KeyCode::E, E], modifier: 0 }),
            'f' => Some( InnerKr { keycodes: [KeyCode::F, E], modifier: 0 }),
            'g' => Some( InnerKr { keycodes: [KeyCode::G, E], modifier: 0 }),
            'h' => Some( InnerKr { keycodes: [KeyCode::H, E], modifier: 0 }),
            'i' => Some( InnerKr { keycodes: [KeyCode::I, E], modifier: 0 }),
            'j' => Some( InnerKr { keycodes: [KeyCode::J, E], modifier: 0 }),
            'k' => Some( InnerKr { keycodes: [KeyCode::K, E], modifier: 0 }),
            'l' => Some( InnerKr { keycodes: [KeyCode::L, E], modifier: 0 }),
            'm' => Some( InnerKr { keycodes: [KeyCode::M, E], modifier: 0 }),
            'n' => Some( InnerKr { keycodes: [KeyCode::N, E], modifier: 0 }),
            'o' => Some( InnerKr { keycodes: [KeyCode::O, E], modifier: 0 }),
            'p' => Some( InnerKr { keycodes: [KeyCode::P, E], modifier: 0 }),
            'q' => Some( InnerKr { keycodes: [KeyCode::Q, E], modifier: 0 }),
            'r' => Some( InnerKr { keycodes: [KeyCode::R, E], modifier: 0 }),
            's' => Some( InnerKr { keycodes: [KeyCode::S, E], modifier: 0 }),
            't' => Some( InnerKr { keycodes: [KeyCode::T, E], modifier: 0 }),
            'u' => Some( InnerKr { keycodes: [KeyCode::U, E], modifier: 0 }),
            'v' => Some( InnerKr { keycodes: [KeyCode::V, E], modifier: 0 }),
            'w' => Some( InnerKr { keycodes: [KeyCode::W, E], modifier: 0 }),
            'x' => Some( InnerKr { keycodes: [KeyCode::X, E], modifier: 0 }),
            'y' => Some( InnerKr { keycodes: [KeyCode::Y, E], modifier: 0 }),
            'z' => Some( InnerKr { keycodes: [KeyCode::Z, E], modifier: 0 }),

            // A to Z
            'A' => Some( InnerKr { keycodes: [KeyCode::A, E], modifier: LSHIFT }),
            'B' => Some( InnerKr { keycodes: [KeyCode::B, E], modifier: LSHIFT }),
            'C' => Some( InnerKr { keycodes: [KeyCode::C, E], modifier: LSHIFT }),
            'D' => Some( InnerKr { keycodes: [KeyCode::D, E], modifier: LSHIFT }),
            'E' => Some( InnerKr { keycodes: [KeyCode::E, E], modifier: LSHIFT }),
            'F' => Some( InnerKr { keycodes: [KeyCode::F, E], modifier: LSHIFT }),
            'G' => Some( InnerKr { keycodes: [KeyCode::G, E], modifier: LSHIFT }),
            'H' => Some( InnerKr { keycodes: [KeyCode::H, E], modifier: LSHIFT }),
            'I' => Some( InnerKr { keycodes: [KeyCode::I, E], modifier: LSHIFT }),
            'J' => Some( InnerKr { keycodes: [KeyCode::J, E], modifier: LSHIFT }),
            'K' => Some( InnerKr { keycodes: [KeyCode::K, E], modifier: LSHIFT }),
            'L' => Some( InnerKr { keycodes: [KeyCode::L, E], modifier: LSHIFT }),
            'M' => Some( InnerKr { keycodes: [KeyCode::M, E], modifier: LSHIFT }),
            'N' => Some( InnerKr { keycodes: [KeyCode::N, E], modifier: LSHIFT }),
            'O' => Some( InnerKr { keycodes: [KeyCode::O, E], modifier: LSHIFT }),
            'P' => Some( InnerKr { keycodes: [KeyCode::P, E], modifier: LSHIFT }),
            'Q' => Some( InnerKr { keycodes: [KeyCode::Q, E], modifier: LSHIFT }),
            'R' => Some( InnerKr { keycodes: [KeyCode::R, E], modifier: LSHIFT }),
            'S' => Some( InnerKr { keycodes: [KeyCode::S, E], modifier: LSHIFT }),
            'T' => Some( InnerKr { keycodes: [KeyCode::T, E], modifier: LSHIFT }),
            'U' => Some( InnerKr { keycodes: [KeyCode::U, E], modifier: LSHIFT }),
            'V' => Some( InnerKr { keycodes: [KeyCode::V, E], modifier: LSHIFT }),
            'W' => Some( InnerKr { keycodes: [KeyCode::W, E], modifier: LSHIFT }),
            'X' => Some( InnerKr { keycodes: [KeyCode::X, E], modifier: LSHIFT }),
            'Y' => Some( InnerKr { keycodes: [KeyCode::Y, E], modifier: LSHIFT }),
            'Z' => Some( InnerKr { keycodes: [KeyCode::Z, E], modifier: LSHIFT }),

            // Shift + row ` to = -> ~!@#$%^&*()_
            '~' => Some( InnerKr { keycodes: [KeyCode::Tilde, E], modifier: LSHIFT }),
            '!' => Some( InnerKr { keycodes: [KeyCode::Num1, E], modifier: LSHIFT }),
            '@' => Some( InnerKr { keycodes: [KeyCode::Num2, E], modifier: LSHIFT }),
            '#' => Some( InnerKr { keycodes: [KeyCode::Num3, E], modifier: LSHIFT }),
            '$' => Some( InnerKr { keycodes: [KeyCode::Num4, E], modifier: LSHIFT }),
            '%' => Some( InnerKr { keycodes: [KeyCode::Num5, E], modifier: LSHIFT }),
            '^' => Some( InnerKr { keycodes: [KeyCode::Num6, E], modifier: LSHIFT }),
            '&' => Some( InnerKr { keycodes: [KeyCode::Num7, E], modifier: LSHIFT }),
            '*' => Some( InnerKr { keycodes: [KeyCode::Num8, E], modifier: LSHIFT }),
            '(' => Some( InnerKr { keycodes: [KeyCode::Num9, E], modifier: LSHIFT }),
            ')' => Some( InnerKr { keycodes: [KeyCode::Num0, E], modifier: LSHIFT }),
            '_'  => Some( InnerKr { keycodes: [KeyCode::Minus, E], modifier: LSHIFT }),
            '+'  => Some( InnerKr { keycodes: [KeyCode::Equals, E], modifier: LSHIFT }),

            // Shift row qwerty, asdfgh and zxcvb -> {}:"|<>?
            '{' => Some ( InnerKr { keycodes: [KeyCode::LeftSquareBracket, E], modifier: LSHIFT }),
            '}' => Some ( InnerKr { keycodes: [KeyCode::RightSquareBracket, E], modifier: LSHIFT }),
            ':' => Some ( InnerKr { keycodes: [KeyCode::Semicolon, E], modifier: LSHIFT }),
            '"' => Some ( InnerKr { keycodes: [KeyCode::SingleQuote, E], modifier: LSHIFT }),
            '|' => Some ( InnerKr { keycodes: [KeyCode::BackSlash, E], modifier: LSHIFT }),
            '<' => Some ( InnerKr { keycodes: [KeyCode::Comma, E], modifier: LSHIFT }),
            '>' => Some ( InnerKr { keycodes: [KeyCode::Period, E], modifier: LSHIFT }),
            '?' => Some ( InnerKr { keycodes: [KeyCode::Slash, E], modifier: LSHIFT }),

            // Other non alpha without modifier ->  `-= [];'\,./
            '`' => Some ( InnerKr { keycodes: [KeyCode::Tilde, E], modifier: 0 }),
            '-' => Some ( InnerKr { keycodes: [KeyCode::Minus, E], modifier: 0 }),
            '=' => Some ( InnerKr { keycodes: [KeyCode::Equals, E], modifier: 0 }),
            '[' => Some ( InnerKr { keycodes: [KeyCode::LeftSquareBracket, E], modifier: 0 }),
            ']' => Some ( InnerKr { keycodes: [KeyCode::RightSquareBracket, E], modifier: 0 }),
            ';' => Some ( InnerKr { keycodes: [KeyCode::Semicolon, E], modifier: 0 }),
           '\'' => Some ( InnerKr { keycodes: [KeyCode::SingleQuote, E], modifier: 0 }),
           '\\' => Some ( InnerKr { keycodes: [KeyCode::BackSlash, E], modifier: 0 }),
            ',' => Some ( InnerKr { keycodes: [KeyCode::Comma, E], modifier: 0 }),
            '.' => Some ( InnerKr { keycodes: [KeyCode::Period, E], modifier: 0 }),
            '/' => Some ( InnerKr { keycodes: [KeyCode::Slash, E], modifier: 0 }),

            _ => return None,
        };

        let mut kr = KeyboardReport {
            keycodes: [0, 0, 0, 0, 0, 0],
            leds: 0,
            modifier: 0,
            reserved: 0,
        };
        kr.keycodes[0] = inner_kr.unwrap().keycodes[0] as u8;
        kr.keycodes[1] = inner_kr.unwrap().keycodes[1] as u8;
        kr.modifier = inner_kr.unwrap().modifier;

        Some(kr)
    }

    fn is_altgr_modifier(&self, c: char) -> bool {
        let s: String<30> = String::try_from("").unwrap();
        s.contains(c)
    }

    fn is_shift_diaeresis_modifier(&self, c: char) -> bool {
        let s: String<26> = String::try_from("").unwrap();
        s.contains(c)
    }

    fn is_shift_cedilla_modifier(&self, c: char) -> bool {
        let s: String<26> = String::try_from("").unwrap();
        s.contains(c)
    }
}
